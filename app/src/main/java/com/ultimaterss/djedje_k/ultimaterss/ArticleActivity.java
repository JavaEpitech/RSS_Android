package com.ultimaterss.djedje_k.ultimaterss;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by djedje_k on 27/03/2017.
 */

public class ArticleActivity extends Activity {
    private TextView titleContent;
    private TextView dateContent;
    private TextView descriptionContent;
    private Button nextContentButton;
    private final static String TAG = "DEBUG";

//    ArticleActivity(FeedDataCell cell) {
//        title = cell.postTitle;
//        date = cell.postDate;
//        description = cell.postDescription;
//        link = cell.postLink;
//    }

    private void initAllContentView() {
        titleContent = (TextView) findViewById(R.id.titleContent);
        dateContent = (TextView) findViewById(R.id.dateContent);
        descriptionContent = (TextView) findViewById(R.id.descriptionContent);
        nextContentButton = (Button) findViewById(R.id.nextContentButton);

        Bundle parameters = getIntent().getExtras();

        titleContent.setText(parameters.getString("title"));
        dateContent.setText(parameters.getString("date"));
        descriptionContent.setText(parameters.getString("description"));


        Log.d(TAG, "Valeur de la description de l'article : {"+ parameters.getString("description") + "} et celle du lien renvoyer : {" + parameters.getString("link") + "}");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_layout);
        initAllContentView();

        nextContentButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getIntent().getExtras().getString("link")));
                startActivity(intent);
            }
        });
    }
}
