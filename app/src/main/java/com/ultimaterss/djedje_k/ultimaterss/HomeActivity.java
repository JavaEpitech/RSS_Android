package com.ultimaterss.djedje_k.ultimaterss;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

//import org.mindrot.jbcrypt.BCrypt;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

// exemple d'url d'image à DL ==> http://farm8.staticflickr.com/7315/9046944633_881f24c4fa_s.jpg
// exemple d'url RSS ==> http://www.lequipe.fr/Xml/actu_rss.xml

public class HomeActivity extends BaseActivity {
    private View mProgressBarView;
    private View mListView;
    private static Context mContext;
    private static ArrayList<FeedDataCell> listData = new ArrayList<FeedDataCell>();
    private static ArrayList<FeedDataCell> tmpListData = new ArrayList<FeedDataCell>();
    private static ArrayList<FeedDataCell> nextListData = new ArrayList<FeedDataCell>();
    private static boolean TMP = false;
    private static final String TAG = "DEBUG";
    private static ListView listView;
    private RssDataController mRssDataController = new RssDataController(this);
    private Button mRefreshButton;
    private Button mAddUrl;
    private static User mUser;
    private HomeActivity mActivity;
    private Integer mNewsFeeds = 0;

    public static Context getContext() {
        return mContext;
    }

    static class ViewHolder {
        TextView postTitleView;
        TextView postDateView;
        ImageView postThumbView;
        ImageView imageView;
        String imageURL;
        Bitmap bitmap;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mContext = this.getApplicationContext();
        mActivity = this;

        // Créé l'utilisateur de la session
        mUser = new User(getIntent().getExtras().getString("email"));
        // Set up your ActionBar
        setUpMyCustomActionBar();

        mAddUrl = (Button) findViewById(R.id.action_bar_add_url_rss);
        mAddUrl.setTextColor(Color.parseColor("#CFCFCF"));

        mRefreshButton = (Button) findViewById(R.id.action_bar_refresh);
        mRefreshButton.setY(40);
        mRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(mUser.getCountUrl() > 0))
                    Toast.makeText(mContext, "Impossible de recharger, aucun contenu n'est chargé! Veuillez ajouter une source", Toast.LENGTH_LONG);
                tmpListData = listData;
                TMP = true;
                for (String url : mUser.getAllUrlSubscribe())
                    new RssDataController(mActivity).execute(url);
//                listView.setAdapter(new FeedItemAdapter(mActivity,R.layout.feed_item, listData));
            }
        });

        mProgressBarView = this.findViewById(R.id.home_progressBar);
        mListView = this.findViewById(R.id.postListView);
        listView = (ListView) this.findViewById(R.id.postListView);
        showProgress(true);
        mRssDataController.execute("http://www.lequipe.fr/Xml/actu_rss.xml");
        FeedItemAdapter itemAdapter = new FeedItemAdapter(this,R.layout.feed_item, listData);
//        listData.notifyAll();
        listView.setAdapter(itemAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FeedDataCell cell = listData.get(position);
                Bundle parameters = new Bundle();
                parameters.putString("title", cell.postTitle);
                parameters.putString("date", cell.postDate);
                parameters.putString("link", cell.postLink);
                parameters.putString("description", cell.postDescription);
                startActivity(new Intent(HomeActivity.this, ArticleActivity.class).putExtras(parameters));
            }
        });
    }

    private void setUpMyCustomActionBar() {
        // Inflate your custom layout
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.home_toolbar,
                null);

        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.action_bar);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        final Button actionBarTitle = (Button) findViewById(R.id.action_bar_add_url_rss);
        actionBarTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPopUpURLRSSContent();
            }
        });

        final Button actionBarSent = (Button) findViewById(R.id.action_bar_refresh);

    }

    private void openPopUpURLRSSContent() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
        alertDialog.setTitle("Ajouter un flux RSS");
        alertDialog.setMessage("Enter un nouveau flux RSS");

        final EditText input = new EditText(HomeActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
//                alertDialog.setIcon(R.drawable.key);

        alertDialog.setPositiveButton("Valider",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String urlInput = input.getText().toString();
                        if (!urlInput.equals("")) {
                            if (addURLRSSContent(urlInput)) {
                                //TODO Erreur de flux RSS , afficher l'erreur (popup?)
                            }
                        }
                    }
                });

        alertDialog.setNegativeButton("Annuler",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private boolean addURLRSSContent(String url) {
//        mRssDataController.execute(url);
        new RssDataController(this).execute(url);
        listView.setAdapter(new FeedItemAdapter(this,R.layout.feed_item, listData));
        return false;
    }

    private static class RssDataController extends AsyncTask<String, Integer, ArrayList<FeedDataCell>> {
        private RSSXMLTag currentTag;
        private HomeActivity myActivity;
        private String urlStr;
        private enum RSSXMLTag {
            TITLE,
            LINK,
            DATE,
            IGNORETAG,
            DESCRIPTION,
        }

        public RssDataController(HomeActivity homeActivity)
        {
            this.myActivity = homeActivity;
        }

        @Override
        protected ArrayList<FeedDataCell> doInBackground(String... params) {
            // TODO Auto-generated method stub
            urlStr = params[0];
            InputStream is = null;
            Log.d(TAG, "RssDataController in HomeAcitivity !");
            ArrayList<FeedDataCell> FeedDataCellList = new ArrayList<FeedDataCell>();
            try {
                URL url = new URL(urlStr);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setReadTimeout(10 * 1000);
                connection.setConnectTimeout(10 * 1000);
                connection.setRequestMethod("GET");
                connection.setDoInput(true);
                connection.connect();
                int response = connection.getResponseCode();
                Log.d("debug", "The response is: " + response);
                is = connection.getInputStream();

                // parse xml after getting the data
                XmlPullParserFactory factory = XmlPullParserFactory
                        .newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(is, null);

                int eventType = xpp.getEventType();
                FeedDataCell pdData = null;
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "EEE, DD MMM yyyy HH:mm:ss +GGGG");
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_DOCUMENT) {

                    } else if (eventType == XmlPullParser.START_TAG) {
                        String xppName = xpp.getName();
                        if (xppName.equals("item")) {
                            pdData = new FeedDataCell();
                            pdData.seenContent = false;
                            currentTag = RSSXMLTag.IGNORETAG;
                        } else if (xppName.equals("title")) {
                            currentTag = RSSXMLTag.TITLE;
                        } else if (xppName.equals("link")) {
                            currentTag = RSSXMLTag.LINK;
                        } else if (xppName.equals("pubDate")) {
                            currentTag = RSSXMLTag.DATE;
                        } else if (xppName.equals("description")) {
                            currentTag = RSSXMLTag.DESCRIPTION;
                        }
                    } else if (eventType == XmlPullParser.END_TAG) {
                        String xppName = xpp.getName();
                        if (xppName.equals("item")) {
                            // format the data here, otherwise format data in
                            // Adapter
//                        Date postDate = dateFormat.parse(pdData.postDate);
//                        pdData.postDate = dateFormat.format(postDate);
                            FeedDataCellList.add(pdData);
                        } else {
                            currentTag = RSSXMLTag.IGNORETAG;
                        }
                    } else if (eventType == XmlPullParser.TEXT) {
                        String content = xpp.getText();
                        //Ajouter .trim à la fin de la ligne au dessus
                        content = content.trim();
                        Log.d("debug", "Content : " + content);
                        if (pdData != null) {
                            switch (currentTag) {
                                case TITLE:
                                    if (content.length() != 0) {
                                        if (pdData.postTitle != null) {
                                            pdData.postTitle += content;
                                        } else {
                                            pdData.postTitle = content;
                                        }
                                    }
                                    break;
                                case LINK:
                                    if (content.length() != 0) {
                                        if (pdData.postLink != null) {
                                            pdData.postLink += content;
                                        } else {
                                            pdData.postLink = content;
                                        }
                                    }
                                    break;
                                case DATE:
                                    if (content.length() != 0) {
                                        if (pdData.postDate != null) {
                                            pdData.postDate += content;
                                        } else {
                                            pdData.postDate = content;
                                        }
                                    }
                                    break;
                                case DESCRIPTION:
                                    if (content.length() != 0) {
                                        if (pdData.postDescription != null) {
                                            pdData.postDescription += content;
                                        } else {
                                            pdData.postDescription = content;
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    eventType = xpp.next();
                }
                Log.v("tst", String.valueOf((FeedDataCellList.size())));
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return FeedDataCellList;
        }

        @Override
        protected void onPostExecute(ArrayList<FeedDataCell> result) {
            // TODO Auto-generated method stub
            Integer newFeeds = 0;
            if (!TMP) {
                for (int i = 0; i < result.size(); i += 1) {//ignore this comment >
                    listData.add(result.get(i));
                }
                mUser.addUrl(urlStr);
                myActivity.showProgress(false);
                ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
            } else {
                for (int i = 0; i < result.size(); i += 1)
                    nextListData.add(result.get(i));
                if (urlStr.equals(mUser.getLastElement())) {
                    ArrayList<FeedDataCell> tmp = new ArrayList<FeedDataCell>();
                    FeedDataCell cellTest = new FeedDataCell();
                    cellTest.postLink = "nouveaux url mon gars";
                    cellTest.postDate = "Thu, 30  Mar 2017 07:03:00 +0200";
                    cellTest.postTitle = "Nouvel Article mon gars";
                    cellTest.postDescription = "une longue description bien longue et ennuyeuse de l'article que tu es en train de lire!";
                    tmp.add(cellTest);
                    nextListData = tmp;
                    newFeeds = checkContentListData();
                    if (newFeeds != 0) {
                        listData = nextListData;
                        nextListData = new ArrayList<FeedDataCell>();
                        Toast.makeText(myActivity, "Il y a " + newFeeds.toString() + "nouveaux articles", Toast.LENGTH_LONG);
                        myActivity.showProgress(false);
                        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
                        listView.setAdapter(new FeedItemAdapter(myActivity,R.layout.feed_item, listData));
                    } else
                        Toast.makeText(myActivity, "Il y a " + newFeeds.toString() + "nouveaux articles", Toast.LENGTH_LONG);
                }
            }
        }

        private Integer checkContentListData() {
//            Integer newFeeds = 0;
            for (FeedDataCell nextCell : nextListData) {
                for (int i = 0; i < tmpListData.size(); i += 1) {
                    FeedDataCell tmpCell = tmpListData.get(i);
                    if (nextCell.postLink.equals(tmpCell.postLink))
                        break;
                    if (i == (tmpListData.size()- 1))
                        myActivity.mNewsFeeds += 1;
                }
            }
            Log.d(TAG, "Il y a " + myActivity.mNewsFeeds.toString() + " nouvel article !");
            return myActivity.mNewsFeeds;
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mListView.setVisibility(show ? View.GONE : View.VISIBLE);
            mListView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mListView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressBarView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressBarView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressBarView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressBarView.setVisibility(show ? View.VISIBLE : View.GONE);
            mListView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}