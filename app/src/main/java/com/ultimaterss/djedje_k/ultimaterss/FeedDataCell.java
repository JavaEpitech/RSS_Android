package com.ultimaterss.djedje_k.ultimaterss;

/**
 * Created by djedje_k on 16/03/2017.
 */


public class FeedDataCell {
    public String postThumbUrl;
    public String postTitle;
    public String postDate;
    public String postLink;
    public String postDescription;
    public Boolean seenContent;
}
