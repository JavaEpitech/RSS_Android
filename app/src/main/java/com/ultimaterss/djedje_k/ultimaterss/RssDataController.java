package com.ultimaterss.djedje_k.ultimaterss;

import android.os.AsyncTask;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by djedje_k on 22/03/2017.
 */

public class RssDataController extends AsyncTask<String, Integer, ArrayList<FeedDataCell>> {
    private RSSXMLTag currentTag;

    private enum RSSXMLTag {
        TITLE,
        LINK,
        DATE,
        IGNORETAG,
        DESCRIPTION,
    }

    @Override
    protected ArrayList<FeedDataCell> doInBackground(String... params) {
        // TODO Auto-generated method stub
        String urlStr = params[0];
        InputStream is = null;
        ArrayList<FeedDataCell> FeedDataCellList = new ArrayList<FeedDataCell>();
        try {
            URL url = new URL(urlStr);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setReadTimeout(10 * 1000);
            connection.setConnectTimeout(10 * 1000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            int response = connection.getResponseCode();
            Log.d("debug", "The response is: " + response);
            is = connection.getInputStream();

            // parse xml after getting the data
            XmlPullParserFactory factory = XmlPullParserFactory
                    .newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(is, null);

            int eventType = xpp.getEventType();
            FeedDataCell pdData = null;
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "EEE, DD MMM yyyy HH:mm:ss +GGGG");
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {

                } else if (eventType == XmlPullParser.START_TAG) {
                    String xppName = xpp.getName();
                    if (xppName.equals("item")) {
                        pdData = new FeedDataCell();
                        currentTag = RSSXMLTag.IGNORETAG;
                    } else if (xppName.equals("title")) {
                        currentTag = RSSXMLTag.TITLE;
                    } else if (xppName.equals("link")) {
                        currentTag = RSSXMLTag.LINK;
                    } else if (xppName.equals("pubDate")) {
                        currentTag = RSSXMLTag.DATE;
                    } else if (xppName.equals("description")) {
                        currentTag = RSSXMLTag.DESCRIPTION;
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    String xppName = xpp.getName();
                    if (xppName.equals("item")) {
                        // format the data here, otherwise format data in
                        // Adapter
//                        Date postDate = dateFormat.parse(pdData.postDate);
//                        pdData.postDate = dateFormat.format(postDate);
                        FeedDataCellList.add(pdData);
                    } else {
                        currentTag = RSSXMLTag.IGNORETAG;
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    String content = xpp.getText();
                    //Ajouter .trim à la fin de la ligne au dessus
                    content = content.trim();
                    Log.d("debug", "Content : " + content);
                    if (pdData != null) {
                        switch (currentTag) {
                            case TITLE:
                                if (content.length() != 0) {
                                    if (pdData.postTitle != null) {
                                        pdData.postTitle += content;
                                    } else {
                                        pdData.postTitle = content;
                                    }
                                }
                                break;
                            case LINK:
                                if (content.length() != 0) {
                                    if (pdData.postLink != null) {
                                        pdData.postLink += content;
                                    } else {
                                        pdData.postLink = content;
                                    }
                                }
                                break;
                            case DATE:
                                if (content.length() != 0) {
                                    if (pdData.postDate != null) {
                                        pdData.postDate += content;
                                    } else {
                                        pdData.postDate = content;
                                    }
                                }
                                break;
                            case DESCRIPTION:
                                if (content.length() != 0) {
                                    if (pdData.postDescription != null) {
                                        pdData.postDescription += content;
                                    } else {
                                        pdData.postDescription = content;
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                eventType = xpp.next();
            }
            Log.v("tst", String.valueOf((FeedDataCellList.size())));
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return FeedDataCellList;
    }

//    @Override
//    protected void onPostExecute(ArrayList<FeedDataCell> result) {
//        // TODO Auto-generated method stub
//        for (int i = 0; i < result.size(); i++) {//ignore this comment >
//            listData.add(result.get(i));
//        }
//
//        postAdapter.notifyDataSetChanged();
//    }
}