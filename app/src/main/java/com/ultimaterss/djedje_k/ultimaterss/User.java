package com.ultimaterss.djedje_k.ultimaterss;

import java.util.ArrayList;

/**
 * Created by djedje_k on 30/03/2017.
 */

public class User {
    private ArrayList<String> allUrlSubscribe = new ArrayList<String>();
    private final String email;

    public User(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public ArrayList<String> getAllUrlSubscribe() {
        return allUrlSubscribe;
    }

    public void addUrl(String newUrl) {
        for (String tmp : allUrlSubscribe)
            if (tmp.equals(newUrl))
                return ;
        allUrlSubscribe.add(newUrl);
    }

    public Integer getCountUrl() {return allUrlSubscribe.size();}

    public String getLastElement() {
        if (getCountUrl() > 0)
            return allUrlSubscribe.get(allUrlSubscribe.size() - 1);
        return null;
    }
}
