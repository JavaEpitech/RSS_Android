package com.ultimaterss.djedje_k.ultimaterss;

/**
 * Created by djedje_k on 16/03/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class FeedItemAdapter extends ArrayAdapter<FeedDataCell> {
    private Activity myContext;
    private ArrayList<FeedDataCell> datas;
    private static final String TAG = "DEBUG";

    public FeedItemAdapter(Context context, int textViewResourceId,
                           ArrayList<FeedDataCell> objects) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        myContext = (Activity) context;
        datas = objects;
    }


    public static class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {
        @Override
        protected ViewHolder doInBackground(ViewHolder... params) {
            // TODO Auto-generated method stub
            //load image directly
            ViewHolder viewHolder = params[0];

            try {
                Log.d(TAG, "Value of imageUrl : " + viewHolder.imageURL);
                URL imageURL = new URL(viewHolder.imageURL);
                viewHolder.bitmap = BitmapFactory.decodeStream(imageURL.openStream());
            } catch (IOException e) {
                // TODO: handle exception
                Log.e("error", "Downloading Image Failed");
                viewHolder.bitmap = null;
            }

            return viewHolder;
        }

        @Override
        protected void onPostExecute(ViewHolder result) {
            // TODO Auto-generated method stub
            if (result.bitmap == null) {
                result.imageView.setImageResource(R.drawable.ic_default_image_feed);
            } else {
                result.imageView.setImageBitmap(result.bitmap);
            }
        }
    }

    private class ViewHolder {
        String imageURL;
        Bitmap bitmap;
        ImageView imageView;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = myContext.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.feed_item, null);
        FeedDataCell myCell = datas.get(position);
        ImageView thumbImageView = (ImageView) rowView.findViewById(R.id.postThumb);

        if (myCell.postThumbUrl == null) {
            thumbImageView.setImageResource(R.drawable.ic_default_image_feed);
        } else {
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) rowView.findViewById(R.id.postThumb);
            viewHolder.imageURL = myCell.postThumbUrl;
            new FeedItemAdapter.DownloadAsyncTask().execute(viewHolder);
        }

        TextView postTitleView = (TextView) rowView.findViewById(R.id.postTitleLabel);
        postTitleView.setText(myCell.postTitle);

        TextView postDateView = (TextView) rowView.findViewById(R.id.postDateLabel);
        postDateView.setText(myCell.postDate);

        if (myCell.seenContent == false) {
            postDateView.setTextColor(Color.DKGRAY);
        }
        return rowView;
    }


}
